import pandas as pd
from datetime import datetime
from sqlite import execute_query,execute_insert, connect_sqlite

#---------------------------------------------------#
#-------- création de la BDD avec SQLITE3 ----------#
#---------------------------------------------------#
bdd_name = "france_co"
conn = connect_sqlite(bdd_name)
cursor = conn.cursor()

print("#------ Création de la BDD ------#")
list_query = [
        """
            DROP TABLE IF EXISTS `certificateur`;
        """,
        """
            CREATE TABLE `certificateur` (
                `id` INTEGER PRIMARY KEY,
                `intitule` VARCHAR(250) UNIQUE NOT NULL
            );
        """,
        """
            DROP TABLE IF EXISTS `diplome`;
        """,        
        """                
            CREATE TABLE `diplome` (
                `id` INTEGER PRIMARY KEY,
                `libele` VARCHAR(250) UNIQUE NOT NULL
            );
        """,
                """
            DROP TABLE IF EXISTS `RNCP`;
        """,        
        """                
            CREATE TABLE `RNCP` (
                `id` INTEGER PRIMARY KEY,
                `code` VARCHAR(50) UNIQUE NOT NULL
            );
        """,
        """
            DROP TABLE IF EXISTS `certificat`;
        """,
        """
            CREATE TABLE `certificat` (
                `id` INTEGER PRIMARY KEY,
                `code_RNCP` INTEGER NOT NULL,
                `intitule` VARCHAR(250) UNIQUE NOT NULL,
                `certificateur_id` INTEGER NOT NULL,
                `diplome_id` INTEGER NOT NULL,
                FOREIGN KEY (`certificateur_id`) REFERENCES `certificateur` (`id`),
                FOREIGN KEY (`diplome_id`) REFERENCES `diplome` (`id`)
                FOREIGN KEY (`code_RNCP`) REFERENCES `RNCP` (`id`)
            );
        """,
        """
            DROP TABLE IF EXISTS `CPNE`;
        """,
        """        
            CREATE TABLE `CPNE` (
                `id` INTEGER PRIMARY KEY,
                `code` INTEGER UNIQUE NOT NULL,
                `intitule` VARCHAR(255) NOT NULL
            );
        """,
        """
            DROP TABLE IF EXISTS `IDCC`;
        """,
        """        
            CREATE TABLE `IDCC` (
                `id` INTEGER PRIMARY KEY,
                `code` INTEGER UNIQUE NOT NULL,
                `CPNE_id` INTEGER NOT NULL,
                FOREIGN KEY (`CPNE_id`) REFERENCES `CPNE` (`id`)
            );
        """,
        """
            DROP TABLE IF EXISTS `date`;
        """,
        """
            CREATE TABLE `date` (
                `id` INTEGER PRIMARY KEY,
                `date` DATETIME UNIQUE NOT NULL
            );
        """,
        """
            DROP TABLE IF EXISTS `tarif`;
        """,
        """
            CREATE TABLE `tarif` (
                `id` INTEGER PRIMARY KEY,
                `tarif` INTEGER UNIQUE NOT NULL
            );
        """,
                """
            DROP TABLE IF EXISTS `statut`;
        """,
        """
            CREATE TABLE `statut` (
                `id` INTEGER PRIMARY KEY,
                `statut` VARCHAR(10) UNIQUE NOT NULL
            );
        """,
        """
            DROP TABLE IF EXISTS `application`;
        """,
        """
            CREATE TABLE `application` (
                `certificat_id` INTEGER NOT NULL,
                `CPNE_id` INTEGER NOT NULL,
                `statut_id` INTEGER NOT NULL,
                `tarif_id` INTEGER NOT NULL,
                `date_id` INTEGER NOT NULL,
                FOREIGN KEY (`certificat_id`) REFERENCES `certificat` (`id`),
                FOREIGN KEY (`CPNE_id`) REFERENCES `CPNE` (`id`)
                FOREIGN KEY (`statut_id`) REFERENCES `statut` (`id`)
                FOREIGN KEY (`tarif_id`) REFERENCES `tarif` (`id`)
                FOREIGN KEY (`date_id`) REFERENCES `date` (`date`)
            );
        """]
for query in list_query:
    execute_query(query, cursor)
print("#------ Fin de la création ------#")

#------------------------------------------------------------------#
#------- Importation des données du excel dans un dataframe -------#
#------------------------------------------------------------------#
print("#------ Création de la DataFrame ------#")
df = pd.ExcelFile("ressource/Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx")
# df = pd.ExcelFile("ressource/test.xlsx")

sheet3 = pd.read_excel(df, 'Onglet 3 - référentiel NPEC', header=3,  dtype = {"NPEC final": int})
sheet3.rename(columns={'Code RNCP': 'code_rncp', 
                   'Libellé de la formation': 'libelle_formation', 
                   'Certificateur* \n(*liste non exhaustive - premier dans la fiche RNCP)': 'libelle_certificateur',
                   'Libellé du Diplôme': 'diplome',
                   'Code CPNE': 'code_cpne',
                   'NPEC final': 'prix',
                   'Statut': 'statut',
                   'Date d\'applicabilité des NPEC** \n(**hors contrats couverts par la valeur d\'amorçage + voir onglet "Me lire")': 'date'
                   },
          inplace=True)

sheet4 = pd.read_excel(df, 'Onglet 4 - CPNE-IDCC',header=2)
sheet4.rename(columns={'Code CPNE': 'code_cpne', 
                   'CPNE': 'cpne', 
                   'IDCC': 'idcc',
                   },
          inplace=True)
print("#------ Fin de la création ------#")

#---------------------------------------------------#
#------- Importation des données dans la BDD -------#
#---------------------------------------------------#
print(f"#------ INSERT CPNE AND IDCC ------#")
for index, row in sheet4.iterrows():
    query = """INSERT OR IGNORE INTO CPNE (code, intitule) VALUES (?, ?);"""
    id_cpne = execute_insert(query, (row['code_cpne'], row['cpne']), cursor)
    execute_insert("INSERT OR IGNORE INTO IDCC (code, CPNE_id) VALUES (?, ?);",(row['idcc'], id_cpne), cursor)
print(f"#------ END INSERT ------#")

print("#------ INSERT du reste ------#")
for index, row in sheet3.iterrows():
    query = """ INSERT INTO certificateur (intitule) VALUES (?) """
    id_certificateur = execute_insert(query, (row['libelle_certificateur'],), cursor)

    query = """ INSERT INTO diplome (libele) VALUES (?) """
    id_diplome = execute_insert(query, (row['diplome'],), cursor)

    query = """ INSERT INTO RNCP (code) VALUES (?) """
    id_rncp = execute_insert(query, (row['code_rncp'],), cursor)

    query = """ INSERT INTO statut (statut) VALUES (?) """
    id_statut = execute_insert(query, (row['statut'],), cursor)
    if not id_statut:
        query = """ SELECT id FROM statut WHERE statut = ?"""
        id_statut = execute_query(query, cursor, (row['statut'],))[0][0]

    query = """ INSERT INTO tarif (tarif) VALUES (?) """
    id_prix = execute_insert(query, (row['prix'],), cursor)
    if not id_prix:
        query = """ SELECT id FROM tarif WHERE tarif = ?"""
        id_prix = execute_query(query, cursor, (row['prix'],))[0][0]
    
    date = row['date']
    if isinstance(date, datetime):
        date_str = date.strftime("%Y-%m-%d %H:%M:%S")
    else:
        date_str = str(date)
    query = """ INSERT INTO date (date) VALUES (?) """
    id_date = execute_insert(query, (date_str,), cursor)
    if not id_date:
        query = """ SELECT id FROM date WHERE date = ?"""
        id_date = execute_query(query, cursor, (date_str,))[0][0]
    
    query = """ SELECT id FROM certificat WHERE intitule = ?"""
    id_certificat = execute_query(query, cursor, (row['libelle_formation'],))
    if not id_certificat:
        if not id_rncp:
            query = """ SELECT id FROM RNCP WHERE code = ?"""
            id_rncp = execute_query(query, cursor, (row['code_rncp'],))[0][0]
        if not id_certificateur:
            query = """ SELECT id FROM certificateur WHERE intitule = ?"""
            id_certificateur = execute_query(query, cursor, (row['libelle_certificateur'],))[0][0]
        if not id_diplome:
            query = """ SELECT id FROM diplome WHERE libele = ?"""
            id_diplome = execute_query(query, cursor, (row['diplome'],))[0][0]
        query = """INSERT INTO certificat (code_RNCP, intitule, certificateur_id, diplome_id) VALUES (?, ?, ?, ?)"""
        id_certificat = execute_insert(query, (id_rncp, row['libelle_formation'], id_certificateur, id_diplome), cursor)
    else: 
        id_certificat = id_certificat[0][0]
        
    query = """SELECT id FROM CPNE WHERE code = ?"""
    id_cpne = execute_query(query, cursor, (row['code_cpne'],))
    
    query = """INSERT INTO application (certificat_id, CPNE_id, statut_id, tarif_id, date_id) VALUES (?, ?, ?, ?, ?)"""
    id_application = execute_insert(query, (id_certificat, id_cpne[0][0], id_statut, id_prix, id_date), cursor)
print(f"#------ END INSERT ------#")
conn.commit()
cursor.close()
conn.close()
    # time python3 init_bdd.py