CREATE TABLE `certificateur` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `intitule` varchar(250) UNIQUE NOT NULL
);

CREATE TABLE `diplome` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `libele` varchar(250) UNIQUE NOT NULL
);

CREATE TABLE `RNCP` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL
);

CREATE TABLE `certificat` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `code_RNCP` int NOT NULL,
  `intitule` varchar(250) NOT NULL,
  `certificateur_id` int NOT NULL,
  `diplome_id` int NOT NULL
);

CREATE TABLE `CPNE` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `code` int NOT NULL,
  `intitule` varchar(255) NOT NULL
);

CREATE TABLE `IDCC` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `code` int NOT NULL,
  `CNPE_id` int NOT NULL
);

CREATE TABLE `date` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `date` datetime
);

CREATE TABLE `tarif` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `tarif` int NOT NULL
);

CREATE TABLE `statut` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `statut` varchar(10) NOT NULL
);

CREATE TABLE `application` (
  `certificat_id` int NOT NULL,
  `CPNE_id` int NOT NULL,
  `statut_id` int NOT NULL,
  `tarif_id` int NOT NULL,
  `date_id` int NOT NULL
);

ALTER TABLE `certificateur` ADD FOREIGN KEY (`id`) REFERENCES `certificat` (`certificateur_id`);

ALTER TABLE `diplome` ADD FOREIGN KEY (`id`) REFERENCES `certificat` (`diplome_id`);

ALTER TABLE `certificat` ADD FOREIGN KEY (`id`) REFERENCES `application` (`certificat_id`);

ALTER TABLE `CPNE` ADD FOREIGN KEY (`id`) REFERENCES `application` (`CPNE_id`);

ALTER TABLE `CPNE` ADD FOREIGN KEY (`id`) REFERENCES `IDCC` (`CNPE_id`);

ALTER TABLE `date` ADD FOREIGN KEY (`id`) REFERENCES `application` (`date_id`);

ALTER TABLE `tarif` ADD FOREIGN KEY (`id`) REFERENCES `application` (`tarif_id`);

ALTER TABLE `statut` ADD FOREIGN KEY (`id`) REFERENCES `application` (`statut_id`);

ALTER TABLE `RNCP` ADD FOREIGN KEY (`id`) REFERENCES `certificat` (`code_RNCP`);
