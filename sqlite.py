import sqlite3
from sqlite3 import Error

#--------------------------------------------------#
#------- Déclaration des fonctions pour BDD -------#
#--------------------------------------------------#
def connect_sqlite(bdd_name):
    return sqlite3.connect(bdd_name+".db")
        
def execute_query(query: str, cursor, params=None, fetch=True):
    try:
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query}")
        return None


def execute_insert(query: str, values: tuple, cursor)->int:
    try:
        cursor.execute(query, values)
        return cursor.lastrowid
    except sqlite3.IntegrityError:
        return None