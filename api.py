import json
from fastapi import FastAPI
from pydantic import BaseModel
from sqlite import connect_sqlite
from datetime import date
from fastapi_pagination import Page, add_pagination, paginate

app = FastAPI(title="RNCP - CPNE")
add_pagination(app)
# uvicorn api:app --reload

bdd_name = "france_co"
conn = connect_sqlite(bdd_name)

@app.post("/head/")
def print_head():
    return ["Code RNCP", "Libellé de la formation", 
            "Certificateur", "Libellé du Diplôme", 
            "Code CPNE", "CPNE", "IDCC", "NPEC final",
            "Statut", "Date d'applicabilité des NPEC"]

class Table(BaseModel):
    RNCP_code: str
    certificat_intitule: str 
    certificateur_intitule: str
    diplome: str
    CPNE_code: int
    CPNE_intitule: str
    IDCC_code: int
    tarif: int
    statut: str
    date: date

@app.get("/datas/{search}", response_model=Page[Table])
def search_datas(search: str):
    conn = connect_sqlite(bdd_name)
    cursor = conn.cursor()
    search_pattern = '%' + search + '%'
    query = """SELECT
        RNCP.code AS RNCP_code,
        certificat.intitule AS certificat_intitule,
        certificateur.intitule AS certificateur_intitule,
        diplome.libele AS diplome_libele,
        CPNE.code AS CPNE_code,  
        CPNE.intitule AS CPNE_intitule,
        IDCC.code AS IDCC_code,  
        tarif.tarif AS tarif,
        statut.statut AS statut,
        date.date AS date
    FROM
        application
    JOIN
        certificat ON certificat.id = application.certificat_id
    JOIN
        RNCP ON certificat.code_RNCP = RNCP.id
    JOIN
        certificateur ON certificat.certificateur_id = certificateur.id
    JOIN
        diplome ON certificat.diplome_id = diplome.id
    JOIN
        CPNE ON application.CPNE_id = CPNE.id
    JOIN
        statut ON application.statut_id = statut.id
    JOIN
        tarif ON application.tarif_id = tarif.id
    JOIN
        date ON application.date_id = date.id
    JOIN
        IDCC ON CPNE.id = IDCC.CPNE_id
    WHERE
        RNCP.code LIKE ?
        OR certificat.intitule LIKE ?
        OR certificateur.intitule LIKE ?
        OR diplome.libele LIKE ?
        OR CPNE.intitule LIKE ?
        OR IDCC.code LIKE ?
        OR statut.statut LIKE ?
        OR CPNE.code LIKE ?;"""
    
    cursor.execute(query, (search_pattern, search_pattern, search_pattern, search_pattern, search_pattern, search_pattern, search_pattern, search_pattern))
    rows = cursor.fetchall()
    conn.close()
    
    results = [Table(
        RNCP_code=row[0],
        certificat_intitule=row[1],
        certificateur_intitule=row[2],
        diplome=row[3],
        CPNE_code=row[4],
        CPNE_intitule=row[5],
        IDCC_code=row[6],
        tarif=row[7],
        statut=row[8],
        date=row[9]
    ) for row in rows]

    return paginate(results)

@app.get("/startup")
def save_openapi_json():
    openapi_data = app.openapi()
    with open("openapi.json", "w") as file:
        json.dump(openapi_data, file)